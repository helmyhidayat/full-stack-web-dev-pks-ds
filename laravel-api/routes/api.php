<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
], function() {
    Route::post('register','RegisterController')->name('register');
    Route::post('regenerateotpcode','RegenerateOtpCodeController')->name('regenerateotp');
    Route::post('verification','VerificationController')->name('verification');
    Route::post('password-update', 'UpdatePasswordController')->name('updatepassword');
    Route::post('login', 'LoginController')->name('login');
});

Route::get('/test', function() {
    return "test";
})->middleware('auth:api');
