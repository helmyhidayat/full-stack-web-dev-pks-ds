<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp = OtpCode::where('otp',$request->otp)->first();
        if ( !$otp ) {
            return response()->json([
                'success' => false,
                'message' => 'Tidak ada OTP yang ditemukan',
                'data'    => $request->otp
            ], 400);
        }

        if ( Carbon::now() > $otp->valid_until ) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code sudah tidak berlaku lagi',
                'data'    => $otp
            ], 400);
        }

        $user = $otp->user;
        
        $user->update([
            'email_verified_at' => Carbon::now(),
        ]);

        $otp->delete();

        return response()->json([
            'success' => true,
            'message' => 'User berhasil diverifikasi',
            'data'    => $user
        ], 200);






    }
}
