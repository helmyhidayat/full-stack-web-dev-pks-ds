// Tugas 1
console.log("----------------")
console.log("Tugas 1")
console.log("----------------")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()

for( let i = 0; i < daftarHewan.length ; i++ )
{
  console.log(daftarHewan[i])
}












// Tugas 2
console.log("\n----------------")
console.log("----------------")
console.log("Tugas 2")
console.log("----------------")
function introduce(masukan) {
  return "Nama saya " + masukan.name + ", umur saya " + masukan.age + " tahun, alamat saya di " + masukan.address + ", dan saya punya hobby yaitu " + masukan.hobby

  // "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)

// Komentar Kosong



// Tugas 3
console.log("\n----------------")
console.log("Tugas 3")
console.log("----------------")
function hitung_huruf_vokal(kata)
{
  vokal = 0
  for (let i = 0 ; i < kata.length ; i++ ) {
    if ( "aiueo".includes( kata[i].toLowerCase() ) ) {
      vokal++
    }
  }
  return vokal

}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)







// Tugas 4
console.log("\n----------------")
console.log("Tugas 4")
console.log("----------------")
function hitung(angka) {
  return angka*2 - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8