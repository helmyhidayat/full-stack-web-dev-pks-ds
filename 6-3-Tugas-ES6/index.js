// Tugas 1, Arrow Function
console.log('\n-------\nTugas 1\n-------')
let luas = (a, b) => a * b
let keliling = (a, b) => 2 * (a + b)
console.log(luas(4,3))
console.log(keliling(4,3))







// Tugas 2, Arrow Function
console.log('\n-------\nTugas 2\n-------')


const newFunction = (firstName, lastName) => {
  return {
    fullName() {
      console.log(firstName + " " + lastName)
    }
  }
}




newFunction("William", "Imoh").fullName()






// Soal 3
console.log('\n-------\nTugas 3\n-------')
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)





// Soal 4
console.log('\n-------\nTugas 4\n-------')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
console.log(...combined)







// Soal 5
console.log('\n-------\nTugas 5\n-------')
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
var before = `Lorem ${view} dolor sit amet, conextetur adipiscing elit, ${planet}`
console.log(before)