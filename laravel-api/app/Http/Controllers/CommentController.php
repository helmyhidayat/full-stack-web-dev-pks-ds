<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    public function index()
    {
        //get data from table posts
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comments
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // $user = auth()->user();

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id,
        ]);

        //success save to database
        if ($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        //make response JSON
        $comment = Comment::where('id', $id)->first();
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $comment
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::where('id', $id)->first();
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if( $comment->user_id != auth()->user()->id )
        {
            return response()->json([
                'success' => false,
                'message' => 'User not authorized lhoo',
            ], 403);
        }

        if ($comment) {

            //update post
            $comment->update([
                'content'     => $request->content,
                'post_id'     => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $comment
            ], 200);
        }

        //data Comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::where('id', $id)->first();

        if( $comment->user_id != auth()->user()->id )
        {
            return response()->json([
                'success' => false,
                'message' => 'User not authorized lhoo',
            ], 403);
        }
        
        if($comment) {

            //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}


