<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
            'password'   => 'required|confirmed|min:6',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if ( $user ) {
            $user->password = Hash::make($request->password);
            $user->save();
            // update(['password', Hash::make($request->password)]);

            return response()->json([
                'success' => true,
                'message' => 'Password berhasil diupdate',
                'data' => $user
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'User tidak ditemukan',
            'data' => $request->email
        ], 400);

    }
}
