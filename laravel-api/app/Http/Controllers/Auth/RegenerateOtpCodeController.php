<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Saving all Request Input
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'email'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // Cek User
        $user = User::where('email', $request->email)->first();

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', '=', $random)->first();
        } while ($check);

        $user->otp_code->update([
            'otp' => $random,
            'valid_until' => Carbon::now()->addMinutes(5),
        ]);

        // Kirim email OTP Code ke email yang diregistrasikan, nanti

        return response()->json([
            'success' => true,
            'message' => 'Data OTP berhasil diregenerate Ulang',
            'data' => [
                'user' => $user,
                'otp_code' => $user->otp_code
            ]
        ]);




    }
}
