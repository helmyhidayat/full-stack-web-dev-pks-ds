<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Admin',
        ]);
        Role::create([
            'name' => 'Super Admin',
        ]);
        Role::create([
            'name' => 'Mega Admin',
        ]);
        Role::create([
            'name' => 'Hyper Admin',
        ]);
        Role::create([
            'name' => 'Super Hyper Mega Admin',
        ]);
    }
}
