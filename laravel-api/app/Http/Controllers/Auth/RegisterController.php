<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
Use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd($request);
        // Saving all Request Input
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            'email' => 'unique:users,email|required|email',
            'username' => 'required|unique:users,username'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            // 'password' => $request->password,
            'email' => $request->email,
            // 'password' => Hash::make($request->password)
        ]);
        // Membuat random number selama ada angka yang sama di OtpCode
        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', '=', $random)->first();
        } while ($check);

        $otp_code = OtpCode::create([
            'otp' => $random,
            'user_id' => $user->id,
            'valid_until' => Carbon::now()->addMinutes(5)
        ]);

        // Kirim email OTP Code ke email yang diregistrasikan, nanti

        return response()->json([
            'success' => true,
            'message' => 'Data berhasil ditambahkan',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
