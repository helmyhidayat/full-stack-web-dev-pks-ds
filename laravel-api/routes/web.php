<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Role;
use App\Post;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/roles', function() {
    $roles = Role::orderBy('name', 'ASC')->get();
    return view('role.index', compact('roles'));
});
Route::get('/posts', function() {
    $posts = Post::orderBy('title', 'ASC')->get();
    return view('post.index', compact('posts'));
});
