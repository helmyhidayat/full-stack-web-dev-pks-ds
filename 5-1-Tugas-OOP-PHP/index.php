<?php
// Kelas Hewan
abstract class Hewan
{
  public $nama;
  public $darah = 50;
  protected $jumlahKaki;
  protected $keahlian;

  public function aktraksi()
  {
    return $this->nama . " sedang " . $this->keahlian . "<br><br>";
  }


}

// Kelas Fight
trait Fight
{
  public $attackPower;
  public $defencePower;

  public function serang($hewan2)
  {
    echo $this->nama . " sedang menyerang " . $hewan2->nama . "<br>";
    $hewan2->diserang($this);
  }
  public function diserang($hewan2)
  {
    echo $this->nama . " diserang " . $hewan2->nama . "<br><br>";
    return $this->darah -= $hewan2->attackPower / $this->defencePower;
    
  }
}

// Kelas Harimau
class Harimau extends Hewan
{
  use Fight;
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = 4;
    $this->keahlian = "lari cepat";
    $this->attackPower = 7;
    $this->defencePower = 8;
  }
}

// Kelas Elang
class Elang extends Hewan
{
  use Fight;
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = 4;
    $this->keahlian = "terbang tinggi";
    $this->attackPower = 10;
    $this->defencePower = 5;
  }
}

$harimau1 = new Harimau("Harimau Pink");
$elang1 = new Elang("Burung Beo");

echo $harimau1->aktraksi();
echo $elang1->aktraksi();
// Cek 1
$harimau1->serang($elang1);
echo "Darah " . $harimau1->nama . " adalah " . $harimau1->darah . "<br>";
echo "Darah " . $elang1->nama . " adalah " . $elang1->darah . "<br><hr>";

// Cek 2
$harimau1->diserang($elang1);
echo "Darah " . $harimau1->nama . " adalah " . $harimau1->darah . "<br>";
echo "Darah " . $elang1->nama . " adalah " . $elang1->darah . "<br><hr>";


// Cek 3
$elang1->diserang($harimau1);
echo "Darah " . $harimau1->nama . " adalah " . $harimau1->darah . "<br>";
echo "Darah " . $elang1->nama . " adalah " . $elang1->darah . "<br><hr>";

// Cek 4
$elang1->serang($harimau1);
echo "Darah " . $harimau1->nama . " adalah " . $harimau1->darah . "<br>";
echo "Darah " . $elang1->nama . " adalah " . $elang1->darah . "<br><hr>";